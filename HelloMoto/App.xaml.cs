﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloMoto
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloMotoPage();
        }

        protected override void OnStart()
        {
            Debug.WriteLine("OnStart Was Called");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("OnSleep Was Called");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine("OnResume Was Called");
            // Handle when your app resumes
        }
    }
}
