﻿using Xamarin.Forms;

namespace HelloMoto
{
    public partial class HelloMotoPage : ContentPage
    {
        int counter = 0;
        public HelloMotoPage()
        {
            InitializeComponent();

        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            TopLabel.Text = IncrementCounter().ToString();

        }
        int IncrementCounter(){
            counter++;
            return counter;
        }
    }
}
